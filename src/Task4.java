import java.util.Arrays;

public class Task4 {
    private String s;
    private String string1;
    private String string2;

    private String sort(String s) {
        char[] tmp = s.toCharArray();
        Arrays.sort(tmp);
        return new String(tmp);
    }

    public boolean permutation(String string1, String string2) {
        if (string1.length() != string2.length()) {
            return false;
        }
        return sort(string1).equals(sort(string2));
    }
}