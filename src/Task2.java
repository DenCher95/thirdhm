import java.util.Random;

public class Task2 {
    private Random randomNumber;
    private int[] array = new int[10];

    private void fillArray() {
        randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt();
        }
    }

    public int[] squareAndSortArray() {
        fillArray();
        for (int i = 0; i < array.length; i++) {
            array[i] *= array[i];
        }
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        return array;
    }
}