public class Person {
    private String lastName;
    private String firstName;
    private String patronymic;

    public Person(String lastName, String firstName, String patronymic) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
    }

    public Person(String fullName) {
        String[] splitArray = fullName.split(" ");
        this.lastName = splitArray[0];
        this.firstName = splitArray[1];
        this.patronymic = splitArray[2];
    }

    @Override
    public String toString() {
        return lastName + " " + firstName + " " + patronymic;
    }
}